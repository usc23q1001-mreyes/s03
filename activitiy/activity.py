# part 1
leap_year = input('Please input a year:\n')

if leap_year.isnumeric():
  leap_year = int(leap_year)
  if leap_year > 0:
    if leap_year % 400 == 0:
      print(f'{leap_year} is a leap year')
    elif leap_year % 100 == 0:
      print(f'{leap_year} is not a leap year')
    elif leap_year % 4 == 0:
      print(f'{leap_year} is a leap year')
    else:
      print(f'{leap_year} is not a leap year')
  else:
    print('No zero or negative values')
else:
  print('please enter a valid number')
  


# part 2
row = int(input('Enter number of rows:\n'))
col = int(input('Enter number of cols:\n'))

for _ in range(row):
  for _ in range(col):
    print("*", end="")
  print()
  

